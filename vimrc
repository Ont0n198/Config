" Format
set encoding=utf-8 fileencodings=
set nocompatible
set backspace=indent,eol,start

" Start files with insert mode
start

" Activate mouse
set mouse=a
set ruler

" Set tab and space
set shiftwidth=4
set softtabstop=4
set tabstop=4
set expandtab
set smartindent
set autoindent

" display colomn number and max 80
set number
set cc=80

" display tab
set list
set listchars=tab:>-

" shortcuts
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {<CR>}<up><end><CR>

colorscheme slate
filetype plugin indent on
syntax on
